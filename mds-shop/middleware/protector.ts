export default defineNuxtRouteMiddleware((to, from) => {

    const emailCookie = useCookie('mail')
    const emailRegex = /^\w+@\w+\.\w+$/g
    const isEmailValid = emailCookie.value?.match(emailRegex)
  
    if (!isEmailValid) {
      return navigateTo('/')
    }
})
  